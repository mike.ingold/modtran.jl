module MODTRAN
	using CSV
	using DataFrames
	using Unitful
	using Unitful.DefaultSymbols: W, μm, cm, sr

	UNITS_Leν = W / ( cm^2 * sr * cm^-1 );
	UNITS_Leλ = W / ( cm^2 * sr *  μm   );
	UNITS_Eeν = W / ( cm^2   *    cm^-1 );
	UNITS_Eeλ = W / ( cm^2   *     μm   );
	
	export import_modtran_raw, import_modtran
	
	# Header terminology: http://modtran.spectral.com/modtran_faq#what_doc
	HEADERS_TAPE7 = ["FREQ",
			 "TOT_TRANS",
			 "PTH_THRML",
			 "THRML_SCT",
			 "SURF_EMIS",
			 "SOL_SCAT",
			 "SING_SCAT",
			 "GRND_RFLT",
			 "DRCT_RFLT",
			 "TOTAL_RAD",
			 "REF_SOL",
			 "SOL_OBS",
			 "DEPTH"];

	# Same structure as TAPE7 but more human-readable
	HEADERS_DESCRIPTIVE = ["Wavenumber",
				"Total_Transmittance",
				"Path_Thermal_WN",
				"Thermal_Scattering_WN",
				"Surface_Emission_WN",
				"SOL_SCAT",
				"Single_Scatter_WN",
				"Ground_Reflected_WN",
				"Direct_Reflected_WN",
				"Total_Radiance_WN",
				"REF_SOL",
				"Solar_Irradiance_at_Observer_WN",
				"Depth"];

	# Import a MODTRAN output file as-is into a DataFrame
	function import_modtran_raw(fname::String)
		df = CSV.read(fname, DataFrame, header=HEADERS_TAPE7,
			skipto=12, delim=" ", ignorerepeated=true, footerskip=1)
		return df
	end # function

	# Import a MODTRAN output file with descriptive field names and specified units
	function import_modtran(fname::String)
		# Import data and update column names
		df = import_modtran_raw(fname);
		name_map = map(x->(x[1]=>x[2]), zip(HEADERS_TAPE7, HEADERS_DESCRIPTIVE));
		rename!(df, name_map)
		
		# Apply explicit units based on MODTRAN specs
		df.Wavenumber *= cm^-1;
		df.Path_Thermal_WN       *= UNITS_Leν;
		df.Thermal_Scattering_WN *= UNITS_Leν;
		df.Surface_Emission_WN   *= UNITS_Leν;
		df.Single_Scatter_WN     *= UNITS_Leν;
		df.Ground_Reflected_WN   *= UNITS_Leν;
		df.Direct_Reflected_WN   *= UNITS_Leν;
		df.Total_Radiance_WN     *= UNITS_Leν;
		df.Solar_Irradiance_at_Observer_WN *= UNITS_Eeν;

		# Convert wavenumber to wavelength
		ν = df.Wavenumber;
		λ = ( 1.0 ./ ν ) .|> μm;
		insertcols!(df, :Wavelength => λ)

		# Convert per-wavenumber to per-wavelength
		Leν2Leλ(Leν) = ( Leν .*  ν.^2 ) .|> UNITS_Leλ;
		insertcols!(df, :Path_Thermal       => Leν2Leλ(df.Path_Thermal_WN))
		insertcols!(df, :Thermal_Scattering => Leν2Leλ(df.Thermal_Scattering_WN)); 
		insertcols!(df, :Surface_Emission   => Leν2Leλ(df.Surface_Emission_WN))
		insertcols!(df, :Single_Scatter     => Leν2Leλ(df.Single_Scatter_WN))
		insertcols!(df, :Ground_Reflected   => Leν2Leλ(df.Ground_Reflected_WN))
		insertcols!(df, :Direct_Reflected   => Leν2Leλ(df.Direct_Reflected_WN))
		insertcols!(df, :Total_Radiance     => Leν2Leλ(df.Total_Radiance_WN))
		Eeν2Eeλ(Eeν) = ( Eeν .*  ν.^2 ) .|> UNITS_Eeλ;
		insertcols!(df, :Solar_Irradiance_at_Observer => Eeν2Eeλ(df.Solar_Irradiance_at_Observer_WN))
		
		return df
	end # function

end # module
