# MODTRAN.jl

This package is intended to simplify working with [MODTRAN](http://modtran.spectral.com/) from Julia.

Currently supported features:
- Importing data produced by MODTRAN4 into a plain DataFrame
- Importing data produced by MODTRAN4 into a Unitful DataFrame

